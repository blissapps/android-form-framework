package com.blissapplications.formframeworks.formvalidatorobject;

import com.blissapplications.formframeworks.forms.IValidator;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: luisramos
 * Date: 09/09/13
 * Time: 18:40
 */
public class FormValidator {

  private ArrayList<IValidator> ruleSet = new ArrayList<IValidator>();

  public void addValidator(IValidator validator){
    ruleSet.add(validator);
  }

  /**
   * Validates the form (the list of IValidators)
   *
   * @return true if the form is valid, false otherwise
   */
  public boolean validateForm(){
    boolean finalResult = true;
    for(IValidator validator : ruleSet){
      finalResult &= validator.validate();
    }
    return finalResult;
  }
}
