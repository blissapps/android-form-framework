package com.blissapplications.formframeworks.forms.fields;

import android.content.Context;
import android.widget.TextView;
import com.blissapplications.formframeworks.R;
import com.blissapplications.formframeworks.forms.IValidator;
import com.blissapplications.formframeworks.forms.IValueValidator;
import com.blissapplications.formframeworks.utils.StringUtils;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Luis
 * Date: 25/08/13
 * Time: 19:37
 */
public class Field implements IValidator {

    private TextView control;
    private boolean required = true;
    private boolean setsErrorMessages = true;
    private ArrayList<IValueValidator> valueValidatorList;
    private ArrayList<String> errorMessages;

    public Field(TextView textView) {
        this(textView, true);
        init();
    }

    public Field(TextView textView, boolean inRequired) {
        control = textView;
        required = inRequired;
        init();
    }

    public Field(TextView textView, boolean inRequired, boolean setsErrorMessages) {
        this(textView, inRequired);
        this.setsErrorMessages = setsErrorMessages;
    }

    private void init() {
        valueValidatorList = new ArrayList<IValueValidator>();
        errorMessages = new ArrayList<String>();
    }

    public void addValueValidator(IValueValidator valueValidator) {
        valueValidatorList.add(valueValidator);
    }

    @Override
    public boolean validate() {
        errorMessages.clear();
        String value = getValue();
        if (StringUtils.invalidString(value)) {
            if (required) {
                warnRequiredFields();
                return false;
            }
        }
        for (IValueValidator validator : valueValidatorList) {
            boolean resultIsValid = validator.validateValue(value);
            if (resultIsValid) {
                continue;
            } else {
                setErrorMessage(validator.getErrorMessage());
                return false;
            }
        }
        return true;
    }

    protected Context getControlContext() {
        return control != null ? control.getContext() : null;
    }

    private void warnRequiredFields() {
        setErrorMessage(getControlContext().getString(R.string.required_field));
    }

    private void setErrorMessage(String message) {
        errorMessages.add(message);
        if (setsErrorMessages) {
            control.setError(message);
        }
    }

    protected String getValue() {
        return this.control.getText().toString();
    }

    public ArrayList<String> getErrorMessages() {
        return errorMessages;
    }
}
