package com.blissapplications.formframeworks.rules;

import android.widget.TextView;
import com.blissapplications.formframeworks.R;
import com.blissapplications.formframeworks.forms.IValidator;
import com.blissapplications.formframeworks.utils.StringUtils;
import org.apache.http.protocol.RequestUserAgent;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Luis
 * Date: 25/08/13
 * Time: 19:45
 */
public class PasswordFieldRule implements IValidator {

    private TextView password1;
    private TextView password2;
    private boolean groupedErrorMessages;

    private ArrayList<String> errorMessages;

    public PasswordFieldRule(TextView textView1, TextView textView2) {
        this.password1 = textView1;
        this.password2 = textView2;
        errorMessages = new ArrayList<String>();
    }

    public PasswordFieldRule(TextView textView1, TextView textView2, boolean groupedErrorMessages) {
        this(textView1, textView2);
        this.groupedErrorMessages = groupedErrorMessages;
    }

    @Override
    public boolean validate() {
        errorMessages.clear();
        String v1 = password1.getText() != null ? password1.getText().toString() : "";
        String v2 = password2.getText() != null ? password2.getText().toString() : "";
        if (groupedErrorMessages) {
            if (StringUtils.invalidString(v1) || StringUtils.invalidString(v2)) {
                errorMessages.add(password2.getContext().getString(R.string.both_required_fields));
            } else if (!v1.equals(v2)) {
                errorMessages.add(password2.getContext().getString(R.string.password_field_rule_error));
            } else {
                return true;
            }
            return false;
        } else {
            if (v1.equals(v2)) {
                return true;
            } else {
                password2.setError(password2.getContext().getString(R.string.password_field_rule_error));
                return false;
            }
        }
    }

    private void addErrorMessages() {
        errorMessages.clear();

    }

    public ArrayList<String> getErrorMessages() {
        return errorMessages;
    }

}
