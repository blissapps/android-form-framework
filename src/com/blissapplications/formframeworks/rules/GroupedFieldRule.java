package com.blissapplications.formframeworks.rules;

import android.widget.TextView;
import com.blissapplications.formframeworks.R;
import com.blissapplications.formframeworks.forms.IValidator;
import com.blissapplications.formframeworks.utils.StringUtils;

import java.util.ArrayList;

/**
 * Created by diogobrito on 14/02/14.
 */
public class GroupedFieldRule implements IValidator {

    private ArrayList<TextView> textViews;
    private ArrayList<String> errorMessages;


    public GroupedFieldRule(ArrayList<TextView> textViews) {
        errorMessages = new ArrayList<String>();
        this.textViews = textViews;
    }

    @Override
    public boolean validate() {
        for (TextView textView : textViews) {
            if (StringUtils.invalidString(textView.getText().toString())) {
                addRequiredErrorMessage();
                return false;
            } else {
                continue;
            }
        }
        return true;
    }


    private void addRequiredErrorMessage() {
        errorMessages.clear();

        if (textViews.size() == 2) {
            errorMessages.add(textViews.get(0).getContext().getString(R.string.both_required_fields));
        } else {
            errorMessages.add(textViews.get(0).getContext().getString(R.string.all_required_fields));
        }
    }

    public ArrayList<String> getErrorMessages() {
        return errorMessages;
    }

}
